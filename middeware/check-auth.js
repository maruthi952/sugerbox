const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const verified = jwt.verify(token, "sugerbox_key");
    next();
  } catch {
    res.status(401).json({
      message: "Unauthorised",
    });
  }
};
