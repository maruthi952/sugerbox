const User = require('../models/user');

const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

exports.createUser = (req, res, next) => {
	bcrypt
		.hash(req.body.password, 10)
		.then((hash) => {
			tasks = [
				{
					name: 'Task1',
					description: 'Task1'
				},
				{
					name: 'Task2',
					description: 'Task2'
				}
			];
			const user = new User({
				email: req.body.email,
				password: hash,
				tasks: tasks
			});
			console.log(user);
			user
				.save()
				.then((result) => {
					res.status(200).json({
						message: 'User with email: ' + result.email + ' has been successfully created'
					});
				})
				.catch((err) => {
					res.status(500).json({
						message: 'Duplicate email found.Please enter another'
					});
				});
		})
		.catch((err) => {
			console.log(err);
		});
};

exports.deleteUser = (req, res, next) => {
	const email = req.params.email;
	User.deleteOne({ email: email })
		.then((result) => {
			if (result.n > 0) {
				res.status(200).json({ message: 'Deletion successful!' });
			} else {
				res.status(401).json({ message: 'Not authorized!' });
			}
		})
		.catch((error) => {
			res.status(500).json({
				message: 'Deleting posts failed!'
			});
		});
};

exports.userLogin = (req, res, next) => {
	let fetchedUser;
	User.findOne({ email: req.body.email })
		.then((user) => {
			if (!user) {
				return res.status(401).json({
					message: 'Invalid user'
				});
			}
			fetchedUser = user;
			return bcrypt.compare(req.body.password, user.password);
		})
		.then((result) => {
			if (!result) {
				return res.status(401).json({
					message: 'Auth failed'
				});
			}
			const token = jwt.sign({ email: fetchedUser.email, userId: fetchedUser._id }, 'sugerbox_key', {
				expiresIn: '1h'
			});
			res.status(200).json({
				token: token,
				expiresIn: 3600,
				userId: fetchedUser.email
			});
		})
		.catch(() => {
			res.status(500).json({
				message: 'user login failed'
			});
		});
};

exports.getUsers = (req, res, next) => {
	const page = +req.query.page || 1;
	ITEMS_PER_PAGE = 2;
	User.find()
		.countDocuments()
		.then((numUsers) => {
			totalItems = numUsers;
			return User.find({}, { email: 1 }).skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE);
		})
		.then((users) => {
			res.status(200).json({
				users: !users.length ? 'No users found' : users,
				message: 'Users fetched successfully'
			});
		})
		.catch((err) => {
			res.status(500).json({
				message: "requested user doesn't available"
			});
		});
};

exports.getUser = (req, res, next) => {
	const email = req.params.email;
	User.findOne({ email: email }, { email: 1, tasks: 1 })
		.then((data) => {
			if (data) {
				res.status(200).json(data);
			} else {
				res.status(404).json({ message: 'User not found' });
			}
		})
		.catch(() => {
			res.status(500).json({
				message: "requested user doesn't available"
			});
		});
};
