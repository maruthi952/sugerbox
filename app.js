const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/user');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
	next();
});

app.use('/api/user', userRoutes);

mongoose
	.connect('mongodb+srv://Maruthi:XXXX@cluster0-bsdvz.mongodb.net/sugerbox')
	.then(() => {
		console.log('connected to database');
		app.listen(8000);
	})
	.catch((err) => {
		console.log('Connection failed!');
	});
