const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	tasks: [
		{
			name: { type: String, required: true },
			description: { type: String, required: true }
		}
	]
});

module.exports = mongoose.model('User', userSchema);
