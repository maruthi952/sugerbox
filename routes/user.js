const express = require('express');

const userController = require('../controllers/user');

const checkAuth = require('../middeware/check-auth');

const router = express.Router();

// creating new user
router.post('/signup', userController.createUser);

// login
router.post('/login', userController.userLogin);

// delete users
router.delete('/delete/:email', checkAuth, userController.deleteUser);

// fetching all users
router.get('', checkAuth, userController.getUsers);

// fetching user and tasks
router.get('/:email', checkAuth, userController.getUser);

module.exports = router;
